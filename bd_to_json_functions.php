<?php
	
	require_once 'conexion_bd.php';
	
	function envia_datos_cxense($numero_registros_ciclo){
		
		$numero_pendientes = no_pendientes();
		echo $max_ciclos = intval(($numero_pendientes/$numero_registros_ciclo)+1);
		$ciclos = 0;
		while ($numero_pendientes > 0 && $max_ciclos >= $ciclos  ){
			$respuesta_genera = genera_json_cxense($numero_registros_ciclo);
			$plainjson_payload = $respuesta_genera[0];
			$array_enviados = $respuesta_genera[1];

			echo $resultado = subir_json_cxense($plainjson_payload);
			
        	flush();
			if($resultado !== FALSE && $numero_pendientes!='false'){		
				$numero_pendientes = update_enviados($array_enviados,$numero_pendientes);
			}else{
				echo $plainjson_payload;
			}
			$ciclos++;
		}
	}
		
	function genera_json_cxense($numero_registros_ciclo){
		$mysqli = conexion_db();
		$array_enviados = NULL;
		$sql = "SELECT * FROM csv_import WHERE enviado = 0 LIMIT $numero_registros_ciclo";
		if ($resultado = $mysqli->query($sql)) {

			$max = $resultado->num_rows;
			$count = 1;		
			$plainjson_payload = '[';
			while ($fila = $resultado->fetch_assoc()) {

				switch ($fila['gender']){
					case 'm' : 
						$gender = 'male';
						break;
					case 'f' : 
						$gender = 'female';
						break;
					case 'sin genero' : 
						$gender = 'unknown';
						break;
				}			
				$array_enviados[]=$fila['id'];
				$plainjson_payload .= 
					'{
						"id":"'.$fila['id_registro'].'",
						"type":"pep",
						"profile":[
							{"group":"pep-club-autos","item":"'.$fila['club-autos'].'"},
							{"group":"pep-rango-edad","item":"'.$fila['rango-edad'].'"},
							{"group":"pep-club-entretenimiento","item":"'.$fila['club-entretenimiento'].'"},
							{"group":"pep-club-restaurantes","item":"'.$fila['club-restaurantes'].'"},
							{"group":"pep-registrado-elpais","item":"'.$fila['registrado-elpais'].'"},
							{"group":"pep-club-calzado","item":"'.$fila['club-calzado'].'"},
							{"group":"pep-club-salud","item":"'.$fila['club-salud'].'"},
							{"group":"pep-suscriptor","item":"'.$fila['suscriptor'].'"},
							{"group":"pep-email","item":"'.$fila['email'].'"},
							{"group":"pep-club-deportivo","item":"'.$fila['club-deportivo'].'"},
							{"group":"pep-gender","item":"'.$gender.'"}
						]
					}';
				if($count<$max){
					$plainjson_payload .= ',';
				}
				$count++;
			}
			$plainjson_payload .= ']';

			$resultado->close();
		}
		$respuesta = NULL;
		$respuesta []=$plainjson_payload;
		$respuesta []=$array_enviados;
		return $respuesta;
	}

	function subir_json_cxense($plainjson_payload){
		$username = "camunoz@elpais.com.co";
		$apikey = "api&user&+ePCP3KZwcHM1//jC2Gogg==";
		$date = date("Y-m-d\TH:i:s.000O");
		$url = 'https://api.cxense.com/profile/user/external/update';
		$signature = hash_hmac("sha256", $date, $apikey);

		$options = array(
			'http' => array(
				'header'  => "Content-Type: application/json; charset=UTF-8\r\n".
							 "X-cXense-Authentication: username=$username date=$date hmac-sha256-hex=$signature\r\n",
				'method'  => 'POST',
				'content' => $plainjson_payload,
			)
		);

		$context  = stream_context_create($options);
		return $result = file_get_contents($url, false, $context);
	}

	function update_enviados($array_enviados,$numero_pendientes){
		$mysqli = conexion_db();
		//var_dump($array_enviados);
		$count = 0;
		$max = count($array_enviados);
		$where = '';
		$enviados = '';
		while($count < $max){
			$where .= "id='$array_enviados[$count]'" ;
			$enviados .= $array_enviados[$count];			
			$count++;
			if($count<$max){
				$where .= ' OR ';
				$enviados .= ',';
			}
		}
		echo "Send: $enviados\n";
		$sql = "UPDATE csv_import SET enviado = '1' WHERE $where";
		if ($resultado = $mysqli->query($sql)) {
			$numero_pendientes_temp = no_pendientes();
			if($numero_pendientes == $numero_pendientes_temp ){
				$numero_pendientes = 'false';
			}else{				
				//echo "$numero_pendientes = $numero_pendientes_temp";
				$numero_pendientes = $numero_pendientes_temp;
			}
		}else{			
			$numero_pendientes = 'false';
		}
		return $numero_pendientes;
	}

	function no_pendientes(){
		$mysqli = conexion_db();
		$resultado = $mysqli->query('SELECT * FROM csv_import WHERE enviado = 0');
		return $resultado->num_rows;
	}

	