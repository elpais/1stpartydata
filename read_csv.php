<?php
	$maxima_duracion_script = 120; //  MINUTOS
	$maxima_duracion_script = $maxima_duracion_script * 60; //  MINUTOS
		
	ini_set('memory_limit','900M');
	set_time_limit($maxima_duracion_script); 
	ini_set('max_execution_time', $maxima_duracion_script);
	// INICIAR PROCESO
	cargar_archivos_csv();
	
	function conexion_db(){
		$hostname = 'localhost';
		$username = 'root';
		$password = 'elpais';
		$database = 'cxense';
		$mysqli = new mysqli($hostname, $username, $password, $database);
		if ($mysqli->connect_errno) {
			printf("Falló la conexión: %s\n", $mysqli->connect_error);
			exit();
		}
		return $mysqli;
	}

	function cargar_archivos_csv(){
		ob_start();
		
		$list_csv_files = list_files();				/// LISTADO COMPLETO DE ARCHIVOS
		$archivos_importados = array_procesados();	/// LISTADO DE ARCHIVOS PROCESADOS
		foreach ($list_csv_files as $filename){
			if(!isset($archivos_importados[$filename])){	
				echo 'Archivo nuevo encontrado: '.$filename."\n";			
				$filearray = read_file($filename);
				$exito = array_to_bd($filearray);
				if($exito == 'true'){
					registrar_procesado($filename);					
				}else{
					echo 'Error leyendo'.$filename;
				}
			}
		}
		$logs = ob_get_contents();
		ob_end_clean();
		if($logs !='')
			write_log($logs);
	}

	function registrar_procesado($filename){
		$mysqli = conexion_db();
		$sql = "INSERT INTO archivos_importados (`nombre_archivo`) VALUES ('$filename')";
		$resultado = $mysqli->query($sql);
		echo 'Finalizado: '.$filename."\n";
	}

	function array_procesados(){
		$mysqli = conexion_db();
		$archivos_importados = null;
		$sql = "SELECT * FROM archivos_importados";
		if ($resultado = $mysqli->query($sql)) {			
			while ($fila = $resultado->fetch_assoc()) {
				$archivos_importados[$fila['nombre_archivo']] = 'OK' ;
			}
		}
		return $archivos_importados;
	}

	function list_files(){
		$list_csv_files = null;
		$directorio = opendir("csv_files");
		while ($archivo = readdir($directorio))	{
			if (!is_dir($archivo) && strrpos($archivo, '.csv')>0){
				$list_csv_files[] = $archivo;
			}
		}
		return $list_csv_files;
	}
	
	function read_file($filename){
		// open the csv file
		$filestring = file_get_contents('csv_files/'.$filename);	// LEO EL ARCHIVO
		$filestring = utf8_decode($filestring);
		$filestring = nl2br($filestring);							// IDENTIFICO SALTO DE PAGINAS
		$filestring = strtolower($filestring);						// TODO A MINUSCULAS
		$array_replace = array("\n","\r\n",'"');		
		$filestring = str_replace($array_replace,"",$filestring);	// REEMPLAZO LOS SALTOS DE PAGINA
		$filearray = explode("<br />", $filestring);				// CREO ARRAY CON LOS RENGLONES
		unset($filearray[0]);
		return $filearray;
	}
	
	function array_to_bd($filearray){
		$exitoso = 'false';
		$mysqli = conexion_db();
		$mysqli->query('TRUNCATE TABLE `csv_import`');
		foreach ($filearray as $element){			// RECORRO LOS RENGLONES
			$row = explode(";", $element);			// CREO ARRAY CON LOS DATOS DE CADA RENGLON
			if(count ($row) ==22){
				$exitoso = 'true';
				$sql = "INSERT INTO csv_import (
								`id_registro`,
								`email`,
								`gender`,
								`rango-edad`,
								`registrado-elpais`,
								`suscriptor`,
								`club-autos`,
								`club-calzado`,
								`club-deportivo`,
								`club-entretenimiento`,
								`club-restaurantes`,
								`club-salud`,
								`vop-belleza-pelo`,
								`vop-belleza-salud`,
								`vop-deporte-implementos`,
								`vop-hogar-cocina`,
								`vop-hogar-refrigeracion`,
								`vop-hogar-viaje`,
								`vop-navidad-navidad`,
								`vop-tecnologia-celular`,
								`vop-tecnologia-computo`,
								`vop-tecnologia-gadgets`
							)VALUES(
								'".trim($row[0])."',
								'".$row[1]."',
								'".$row[2]."',
								'".$row[3]."',
								'".$row[4]."',
								'".$row[5]."',
								'".$row[6]."',
								'".$row[7]."',
								'".$row[8]."',
								'".$row[9]."',
								'".$row[10]."',
								'".$row[11]."',
								'".$row[12]."',
								'".$row[13]."',
								'".$row[14]."',
								'".$row[15]."',
								'".$row[16]."',
								'".$row[17]."',
								'".$row[18]."',
								'".$row[19]."',
								'".$row[20]."',
								'".trim($row[21])."'
							)";
				
				if ($mysqli->query($sql) === FALSE) {				// SE REALIZA INSERCION EN LA TABLA TEMPORAL
					printf("\nError: %s\n", $mysqli->error);
				}else{
					echo 'Ok: '.trim($row[0])."\n";
				}
			}
		}
		return $exitoso;
	}

	function write_log($cadena){
		echo '<pre>'.$cadena.'</pre>';
		$arch = fopen(realpath( '.' )."/logs/log_".date("Y-m-d His").".txt", "a+");
		fwrite($arch, $cadena);
		fclose($arch);
	}