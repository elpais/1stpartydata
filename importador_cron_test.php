<?php
	
	header("Content-Type:text/html; charset=utf-8"); 
	$maxima_duracion_script = 60; //  MINUTOS
	$maxima_duracion_script = $maxima_duracion_script * 60; //  MINUTOS
		
	ini_set('memory_limit','200M');
	set_time_limit($maxima_duracion_script); 
	ini_set('max_execution_time', $maxima_duracion_script);

	require_once 'conexion_bd.php';
	require_once 'read_csv_functions.php';
	require_once 'bd_to_json_functions.php';

	$numero_registros_ciclo = '100';
		
	ob_start();
/*
	$tiempo_inicio = microtime(TRUE);
	cargar_archivos_csv();
	$tiempo_fin = microtime(TRUE);
	$tiempo_s = round(($tiempo_fin - $tiempo_inicio), 2);
	$tiempo_m = round(($tiempo_fin - $tiempo_inicio)/60, 2);
	echo "Tiempo empleado en carga de archivos: $tiempo_m Min ($tiempo_s Seg.)\n";
*/
	$tiempo_inicio = microtime(TRUE);
	envia_datos_cxense($numero_registros_ciclo);$tiempo_fin = microtime(TRUE);
	$tiempo_s = round(($tiempo_fin - $tiempo_inicio), 2);
	$tiempo_m = round(($tiempo_fin - $tiempo_inicio)/60, 2);
	echo "Tiempo empleado en carga a CXense: $tiempo_m Min ($tiempo_s Seg.)\n";
	$logs = ob_get_contents();
	ob_end_clean();

	if($logs !=''){
		write_log($logs);
	}