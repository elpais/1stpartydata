-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 16-08-2016 a las 16:11:08
-- Versión del servidor: 5.5.42
-- Versión de PHP: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cxense`
--
CREATE DATABASE IF NOT EXISTS `cxense` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cxense`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos_importados`
--

DROP TABLE IF EXISTS `archivos_importados`;
CREATE TABLE `archivos_importados` (
  `id` int(11) NOT NULL,
  `nombre_archivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncar tablas antes de insertar `archivos_importados`
--

TRUNCATE TABLE `archivos_importados`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `csv_import`
--

DROP TABLE IF EXISTS `csv_import`;
CREATE TABLE `csv_import` (
  `id` int(255) NOT NULL,
  `id_registro` int(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `rango-edad` varchar(255) NOT NULL,
  `registrado-elpais` varchar(5) NOT NULL,
  `suscriptor` varchar(5) NOT NULL,
  `club-autos` varchar(5) NOT NULL,
  `club-calzado` varchar(5) NOT NULL,
  `club-deportivo` varchar(5) NOT NULL,
  `club-entretenimiento` varchar(5) NOT NULL,
  `club-restaurantes` varchar(5) NOT NULL,
  `club-salud` varchar(5) NOT NULL,
  `enviado` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncar tablas antes de insertar `csv_import`
--

TRUNCATE TABLE `csv_import`;
--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivos_importados`
--
ALTER TABLE `archivos_importados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `csv_import`
--
ALTER TABLE `csv_import`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_registro` (`id_registro`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `archivos_importados`
--
ALTER TABLE `archivos_importados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `csv_import`
--
ALTER TABLE `csv_import`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
