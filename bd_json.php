<?php
	$maxima_duracion_script = 120; //  MINUTOS
	$maxima_duracion_script = $maxima_duracion_script * 60; //  MINUTOS
	echo 'entra';	
	ini_set('memory_limit','500M');
	set_time_limit($maxima_duracion_script); 
	ini_set('max_execution_time', $maxima_duracion_script);

	$debug = false;
	$numero_registros = '100';	
	
	$exit = 'false';
	$numero_pendientes = no_pendientes();
	while ($numero_pendientes > 0 && $exit == 'false'){
		$respuesta_genera = genera_json_cxense($numero_registros);
		$plainjson_payload = $respuesta_genera[0];
		$array_enviados = $respuesta_genera[1];
		
		$resultado = subir_json_cxense($plainjson_payload);

		if ($debug == true) {
			echo '<pre>'.$plainjson_payload.'</pre>';
			exit();
		}
		if($resultado !== FALSE && $numero_pendientes!='false'){		
			$numero_pendientes = update_enviados($array_enviados,$numero_pendientes);
		}else{
			//echo '<pre>'.$plainjson_payload.'</pre>';
		}
	}

	// AÑADIR PAGINACION U OTRA FORMA DE CONTINUAR PARA SALTAR LOS ERRORES ENVIADO A CXENSE
	function genera_json_cxense($numero_registros){
		$mysqli = conexion_db();
		$array_enviados = NULL;
		$sql = "SELECT * FROM csv_import WHERE enviado = 0 LIMIT $numero_registros";
		if ($resultado = $mysqli->query($sql)) {

			$max = $resultado->num_rows;
			$count = 1;		
			$plainjson_payload = '[';
			while ($fila = $resultado->fetch_assoc()) {
				if(strlen ( $fila['email']) <= 40 ){
					switch ($fila['gender']){
						case 'm' : 
							$gender = 'male';
							break;
						case 'f' : 
							$gender = 'female';
							break;
						case 'sin genero' : 
							$gender = 'unknown';
							break;
					}			
					$array_enviados[]=$fila['id'];
					$plainjson_payload_temp = 
						'{
							"id":"'.$fila['email'].'",
							"type":"pep",
							"profile":[
								{"group":"pep-club-autos","item":"'.$fila['club-autos'].'"},
								{"group":"pep-rango-edad","item":"'.$fila['rango-edad'].'"},
								{"group":"pep-club-entretenimiento","item":"'.$fila['club-entretenimiento'].'"},
								{"group":"pep-club-restaurantes","item":"'.$fila['club-restaurantes'].'"},
								{"group":"pep-registrado-elpais","item":"'.$fila['registrado-elpais'].'"},
								{"group":"pep-club-calzado","item":"'.$fila['club-calzado'].'"},
								{"group":"pep-club-salud","item":"'.$fila['club-salud'].'"},
								{"group":"pep-suscriptor","item":"'.$fila['suscriptor'].'"},
								{"group":"pep-email","item":"'.$fila['email'].'"},
								{"group":"pep-club-deportivo","item":"'.$fila['club-deportivo'].'"},
								{"group":"pep-vop-belleza-pelo","item":"'.$fila['vop-belleza-pelo'].'"},
								{"group":"pep-vop-belleza-salud","item":"'.$fila['vop-belleza-salud'].'"},
								{"group":"pep-vop-deporte-implementos","item":"'.$fila['vop-deporte-implementos'].'"},
								{"group":"pep-vop-hogar-cocina","item":"'.$fila['vop-hogar-cocina'].'"},
								{"group":"pep-vop-hogar-refrigeracion","item":"'.$fila['vop-hogar-refrigeracion'].'"},
								{"group":"pep-vop-hogar-viaje","item":"'.$fila['vop-hogar-viaje'].'"},
								{"group":"pep-vop-navidad-navidad","item":"'.$fila['vop-navidad-navidad'].'"},
								{"group":"pep-vop-tecnologia-celular","item":"'.$fila['vop-tecnologia-celular'].'"},
								{"group":"pep-vop-tecnologia-computo","item":"'.$fila['vop-tecnologia-computo'].'"},
								{"group":"pep-vop-tecnologia-gadgets","item":"'.$fila['vop-tecnologia-gadgets'].'"},
								{"group":"pep-gender","item":"'.$gender.'"}
							]
						}';
					$plainjson_payload .= utf8_decode($plainjson_payload_temp);
					if($count<$max){
						$plainjson_payload .= ',';
					}
				}else{
					update_rechazado($fila['id_registro']);
				}
				$count++;
			}
			$plainjson_payload .= ']';

			$resultado->close();
		}
		$respuesta = NULL;
		$respuesta []=$plainjson_payload;
		$respuesta []=$array_enviados;
		return $respuesta;
	}

	function subir_json_cxense($plainjson_payload){
		$username = "camunoz@elpais.com.co";
		$apikey = "api&user&+ePCP3KZwcHM1//jC2Gogg==";
		$date = date("Y-m-d\TH:i:s.000O");
		$url = 'https://api.cxense.com/profile/user/external/update';
		$signature = hash_hmac("sha256", $date, $apikey);
		
		$options = array(
			'http' => array(
				'header'  => "Content-Type: application/json; charset=UTF-8\r\n".
							 "X-cXense-Authentication: username=$username date=$date hmac-sha256-hex=$signature\r\n",
				'method'  => 'POST',
				'content' => $plainjson_payload,
			)
		);

		$context  = stream_context_create($options);
		return $result = file_get_contents($url, false, $context);
	}

	function conexion_db(){
		$hostname = 'localhost';
		$username = 'root';
		$password = 'elpais';
		$database = 'cxense';
		$mysqli = new mysqli($hostname, $username, $password, $database);
		if ($mysqli->connect_errno) {
			printf("Falló la conexión: %s\n", $mysqli->connect_error);
			exit();
		}
		return $mysqli;
	}

	function update_enviados($array_enviados,$numero_pendientes){
		$mysqli = conexion_db();
		//var_dump($array_enviados);
		$count = 0;
		$max = count($array_enviados);
		$where = '';
		$enviados = '';
		while($count < $max){
			$where .= "id='$array_enviados[$count]'" ;
			$enviados .= $array_enviados[$count];			
			$count++;
			if($count<$max){
				$where .= ' OR ';
				$enviados .= ',';
			}
		}
		echo "ID Enviados: $enviados\n";
		$sql = "UPDATE csv_import SET enviado = '1' WHERE $where";
		if ($resultado = $mysqli->query($sql)) {
			$numero_pendientes_temp = no_pendientes();
			if($numero_pendientes == $numero_pendientes_temp ){
				$numero_pendientes = 'false';
			}else{				
				//echo "$numero_pendientes = $numero_pendientes_temp";
				$numero_pendientes = $numero_pendientes_temp;
			}
		}else{			
			$numero_pendientes = 'false';
		}
		return $numero_pendientes;
	}

	function update_rechazado($id_rechazado){
		$mysqli = conexion_db();		
		echo "ID Rechazado: $id_rechazado\n";
		$sql = "UPDATE csv_import SET enviado = '2' WHERE id_registro= '$id_rechazado'";
		$resultado = $mysqli->query($sql);
	}

	function no_pendientes(){
		$mysqli = conexion_db();
		$resultado = $mysqli->query('SELECT * FROM csv_import WHERE enviado = 0');
		return $resultado->num_rows;
	}

	