<?php

	function cargar_archivos_csv(){
		
		$list_csv_files = list_files();				/// LISTADO COMPLETO DE ARCHIVOS
		$archivos_importados = array_procesados();	/// LISTADO DE ARCHIVOS PROCESADOS
		foreach ($list_csv_files as $filename){
			if(!isset($archivos_importados[$filename])){	
				echo 'Archivo nuevo encontrado: '.$filename."\n";			
				$filearray = read_file($filename);
				$exito = array_to_bd($filearray);
				if($exito == 'true'){
					registrar_procesado($filename);					
				}else{
					echo 'Error leyendo'.$filename."\n";
				}
			}
		}
	}

	function registrar_procesado($filename){
		$mysqli = conexion_db();
		$sql = "INSERT INTO archivos_importados (`nombre_archivo`) VALUES ('$filename')";
		$resultado = $mysqli->query($sql);
		echo 'Finalizado: '.$filename."\n";
	}

	function array_procesados(){
		$mysqli = conexion_db();
		$archivos_importados = null;
		$sql = "SELECT * FROM archivos_importados";
		if ($resultado = $mysqli->query($sql)) {			
			while ($fila = $resultado->fetch_assoc()) {
				$archivos_importados[$fila['nombre_archivo']] = 'OK' ;
			}
		}
		return $archivos_importados;
	}

	function list_files(){
		$list_csv_files = null;
		$directorio = opendir("csv_files");
		while ($archivo = readdir($directorio))	{
			if (!is_dir($archivo) && strrpos($archivo, '.csv')>0){
				$list_csv_files[] = $archivo;
			}
		}
		return $list_csv_files;
	}
	
	function read_file_new($filename){
		// open the csv file
		//$filestring = readfile('csv_files/'.$filename);	// LEO EL ARCHIVO
		$filestring = file_get_contents('csv_files/'.$filename);	// LEO EL ARCHIVO
		$filestring = str_replace('"','',$filestring);	// REEMPLAZO LOS SALTOS DE PAGINA
		$filestring = nl2br($filestring);							// IDENTIFICO SALTO DE PAGINAS
		$filestring = strtolower($filestring);						// TODO A MINUSCULAS
		$array_replace = array("\n","\r\n");		
		$filestring = str_replace($array_replace,"{.}",$filestring);	// REEMPLAZO LOS SALTOS DE PAGINA
		$array_replace = array("\n","\r\n",'"',"\x0B","<br />");		
		$filestring = str_replace($array_replace,"",$filestring);	// REEMPLAZO LOS SALTOS DE PAGINA
		$filestring = filter_var($filestring, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
		
		//echo $filestring;
		$filearray = explode("{.}", $filestring);				// CREO ARRAY CON LOS RENGLONES
		//$filearray = explode("<br />", $filestring);				// CREO ARRAY CON LOS RENGLONES
		unset($filearray[0]);
		//var_dump($filearray);
		return $filearray;
	}

	function read_file($filename){
		// open the csv file
		$filestring = file_get_contents('csv_files/'.$filename);	// LEO EL ARCHIVO
		
		$filestring = str_replace('"','',$filestring);	// REEMPLAZO LOS SALTOS DE PAGINA
		$filestring = nl2br($filestring);							// IDENTIFICO SALTO DE PAGINAS
		$filestring = strtolower($filestring);						// TODO A MINUSCULAS
		$array_replace = array("\n","\r\n");		
		$filestring = str_replace($array_replace,"",$filestring);	// REEMPLAZO LOS SALTOS DE PAGINA
		$filearray = explode("<br />", $filestring);				// CREO ARRAY CON LOS RENGLONES
		unset($filearray[0]);
		return $filearray;
	}
	
	function array_to_bd($filearray){
		$exitoso = 'false';
		$mysqli = conexion_db();
		//var_dump($filearray);
		foreach ($filearray as $element){			// RECORRO LOS RENGLONES
			$row = explode(";", $element);			// CREO ARRAY CON LOS DATOS DE CADA RENGLON
			//var_dump($row);
			if(count ($row) ==12){
				$exitoso = 'true';
				$sql = 'INSERT INTO csv_import (
								`id_registro`,
								`email`,
								`gender`,
								`rango-edad`,
								`registrado-elpais`,
								`suscriptor`,
								`club-autos`,
								`club-calzado`,
								`club-deportivo`,
								`club-entretenimiento`,
								`club-restaurantes`,
								`club-salud`
							)VALUES(
								"'.filter_var($row[0], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH).'",
								"'.filter_var($row[1], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH).'",
								"'.filter_var($row[2], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH).'",
								"'.filter_var($row[3], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH).'",
								"'.$row[4].'",
								"'.$row[5].'",
								"'.$row[6].'",
								"'.$row[7].'",
								"'.$row[8].'",
								"'.$row[9].'",
								"'.$row[10].'",
								"'.trim($row[11]).'"
							)';
				//echo '<pre>'.$sql.'<pre>';
				if ($mysqli->query($sql) === FALSE) {				// SE REALIZA INSERCION EN LA TABLA TEMPORAL
					printf("\nError: %s\n", $mysqli->error);
				}else{
					echo 'Id:'.trim($row[0])."\n";
				}
			}else{
				echo "Sin formato: $element \n";
			}
		}
		return $exitoso;
	}

	function write_log($cadena){
		$cadena = str_replace("\0", "", $cadena);
		
		echo '<pre>'.$cadena.'</pre>';
		$file = fopen(realpath( '.' )."/logs/log_".date("Y-m-d His").".txt", "wb");
		fwrite($file, pack("CCC",0xef,0xbb,0xbf));

		fwrite($file, $cadena);
		
		fclose($file);
	}